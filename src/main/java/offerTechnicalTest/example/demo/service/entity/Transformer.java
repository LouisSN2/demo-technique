package offerTechnicalTest.example.demo.service.entity;

import offerTechnicalTest.example.demo.model.entity.UserEntity;
import offerTechnicalTest.example.demo.model.rest.Registration;
import offerTechnicalTest.example.demo.model.rest.User;
import org.springframework.stereotype.Service;

@Service
public class Transformer {

    public Transformer() {
    }

    public UserEntity transformUserToUserEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setCountry(user.getUserCountry());
        userEntity.setBirthDate(user.getUserBirthDate());
        userEntity.setGender(user.getUserGender());
        userEntity.setName(user.getUserName());
        userEntity.setPhoneNumber(user.getUserPhoneNumber());
        userEntity.setCreatedAt();
        return userEntity;
    }

    public Registration transformUserEntityToRegistration(UserEntity userEntity) {
        Registration registration = new Registration();
        registration.setUserName(userEntity.getName());
        registration.setUserCountry(userEntity.getCountry());
        registration.setUserBirthDate(userEntity.getBirthDate());
        registration.setUserGender(userEntity.getGender());
        registration.setUserPhoneNumber(userEntity.getPhoneNumber());
        registration.setRegistrationDate(userEntity.getCreatedAt());
        return registration;
    }
}
