# API SWIT

_Swit est une application de workflows permettant le suivi de fiches de factures.
Chaque workflow est défini pour un domaine représentant un processus métier. Il est possible de créer fiches, gérer les workflows ainsi que les droits d'administrations._

_L'API SWIT est une API respectant l'architecture CLEAN, devant répondre aux besoins de l'application SWIT._


![image](./Assets/projet.drawio.png)

# Sommaire
* ##  1 - Prérequis
  * ### A - Description
  * ### B - [Guide d'installation](./Assets/Readme/Install/Main.md)
* ## 2 - Architecture
    * ### A - Clean Architecture
    * ### B - Découpage API
* ## 3 - Bogus
* ## 4 - POSTMAN
* ## 5 - OKAPI
* ## 6 - Utils
    * ### A - Commandes

___

* ##  1 - Prérequis
    * ### A - Description
        | Technologie  | Version          | Lien |
        | :---------------: |:---------------:| :-----:|
        | Dotnet  |   v6.0        |  [Clickez ici](https://dotnet.microsoft.com/en-us/download/dotnet/6.0) |
        | SQL Express   | v2019             |   [Clickez ici](https://www.microsoft.com/fr-FR/download/details.aspx?id=101064) |
        | SQL Server Management Studio  | v18         |    [Clickez ici](https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16) |
        | Visual Studio Community  | Latest         |    [Clickez ici](https://visualstudio.microsoft.com/fr/vs/community/) |
    * ### B - Guide d'installation
        []()
        Dans un premier temps, il est important d'avoir créé au préalable un dossier _Applications_ comme suit :

        ![image](./Assets/folders.drawio.png)

        Toutes les technologies suivantes doivent être installées dans le dossier Applications créé ci-dessus.
        * ##### 1 - Dotnet
             []()
            Pour l'installation Dotnet, rendez vous sur ce lien https://dotnet.microsoft.com/en-us/download/dotnet/6.0    ou [clickez ici](https://dotnet.microsoft.com/en-us/download/dotnet/6.0). Ensuite, veuillez suivre les étapes suivantes :

            ![image](./Assets/dotnet.drawio.png)

            Après avoir choisi votre version de Windows, un téléchargement s'est lancé :

            ![image](./Assets/dotnet-ddl.drawio.png)

            Lancez l'installation grâce à ce .EXE .

            ![image](./Assets/dotnet-install.drawio.png)

            Une fois l'installation terminée, Windows ne reconnait pas toujours Dotnet automatiquement. Pour être plus précis, si vous essayez de lancer une commande Dotnet cela crashera automatiquement. Pour s'assurer que Dotnet est bien installé sur votre environnement de travail, ouvrez une invite de commande et lancez la commande suivante :
            ```console
            dotnet --version
            ```
            ![image](./Assets/dotnet-verif.drawio.png)

            Toutefois, si une erreur apparait lors de l'exécution de cette commande, veuillez suivre ces étapes :

            ![image](./Assets/dotnet-env1.drawio.png)
            ![image](./Assets/dotnet-env2.drawio.png)

            Cela permet à Windows de connaître l'emplacement de Dotnet, et de comprendre comment l'utiliser. On est venu modifier la variable       d'environnement _PATH_ de Windows avec cet ajout : 
            ```
            %USERPROFILE%\.dotnet\tools
            ```
            A noter que chaque ajout dans la variable d'environnement _PATH_ doivent être séparés par des ' ; '. Ce qui pourrait donner :
            ```
            AVANT
            C:/variable/bidon
            
            APRES
            C:/variable/bidon;%USERPROFILE%\.dotnet\tools
            ```
            Vous pouvez désormais effectuer la commande précédente, tout devrait rentrer dans l'ordre.
        * ##### 2 - SQL Express
            []()
            **SQL Server Express** est la version gratuite de Microsoft SQL Server, un système de gestion de base de données (SGBD) incorporant entre autres un SGBDR (SGBD relationnel ») développé et proposé au téléchargement par la société Microsoft. Il fonctionne sous les systèmes d'exploitation Microsoft Windows ainsi que sur les systèmes Linux depuis la version 2017. Contrairement à Microsoft SQL Server, SQL Server Express est limité à la prise en charge de 4 cœurs, 4 Go de mémoire vive, avec des bases d'une taille de 10 Go maximum.
            
            Pour l'installation SQL Express, rendez vous sur ce lien https://www.microsoft.com/fr-FR/download/details.aspx?id=101064    ou [clickez ici](https://www.microsoft.com/fr-FR/download/details.aspx?id=101064). Ensuite, veuillez suivre les étapes suivantes :

            ![image](./Assets/SQLE-DDL.drawio.png)

            Un téléchargement s'est lancé :

            ![image](./Assets/SQLE-INSTALL.drawio.png)

        * ##### 3 - SQL Server Management Studio
            []()
            **SQL Server Management Studio** (SSMS) est un environnement intégré développé par Microsoft pour la gestion des infrastructures SQL, de SQL Server à Azure SQL Database. SSMS fournit des outils permettant de configurer, de superviser et d’administrer des instances de SQL Server et des bases de données. SSMS permet de déployer, superviser et mettre à niveau les composants de la couche Données utilisés par vos applications, ainsi que pour créer des requêtes et des scripts.
            
            Pour l'installation SQL Server Management Studio, rendez vous sur ce lien https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16    ou [clickez ici](https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16). Ensuite, veuillez suivre les étapes suivantes :

            ![image](./Assets/SQL-SMS-ddl.drawio.png)

            Un téléchargement s'est lancé :

            ![image](./Assets/SQL-SMS-INSTALL.drawio.png)

        * ##### 4 - Visual Studio Community
            []()
            Visual Studio Community est un IDE (Environnement de développement Intégré) gratuit développé par Microsoft
            Pour l'installation Visual Studio Community, rendez vous sur ce lien https://visualstudio.microsoft.com/fr/vs/community/    ou [clickez ici](https://visualstudio.microsoft.com/fr/vs/community/). Ensuite, veuillez suivre les étapes suivantes :

            ![image](./Assets/VSC-DDL.drawio.png)

            Un téléchargement s'est lancé :

            ![image](./Assets/VSC-INSTALL.drawio.png)

    * ##  2 - Architecture
        * ### A - Clean Architecture
            []()
            L’architecture propre met la logique métier et le modèle d’application au centre même de l’application. Dans une architecture propre, les responsabilités de chaque projet sont clairement établies. À cet effet, certains types sont communs à chaque projet et vous trouverez souvent plusieurs dossiers correspondant à ces types dans le projet en question. La couche Noyau de l’application contient le modèle métier, qui définit les entités, les services et les interfaces. Ces interfaces incluent les abstractions des opérations à effectuer à l’aide de la couche Infrastructure, par exemple l’accès aux données, l’accès au système de fichiers, les appels réseau, etc.
            En d'autres termes, cette architecture est destinée à tous ceux qui veulent développer une API robuste, testable, scalable et qui peut facilement être comprise (et reprise !) par d'autres développeurs. Modifiez-vous la base de données ou l’interface utilisateur, le cœur du système (règles métier/domaine) ne doit pas être modifié. Cela signifie que les dépendances externes sont complètement remplaçables.
            
            Cette architecture se décompose en 4 couches distinctes: _Domain, Application, Infrastructure et Presentation_.

            ![image](./Assets/clean.png)
            
            * Le Domain est indépendant
            * L'Application référence le Domain
            * L'Infrastructure référence l'Application
            * La couche Presentation a la particularité de référencer la couche Infrastructure et l'Application.
        * ### B - Découpage API
            []()
            L'API SWIT est basé sur le modèle de l'architecture CLEAN, et peut être grossièrement découpée ainsi :

            ![image](./Assets/decoupage.drawio.png)

            ![image](./Assets/schemas.drawio.png)
            
    * ## 4 - POSTMAN
        []()
         Postman est un logiciel gratuit qui permet d'effectuer des requêtes API sans coder. Cette interface graphique est utilisée par de nombreux développeurs. Elle facilite la construction de nos requêtes. C’est donc l’outil idéal pour tester des API sans devoir utiliser de code.
         
         Vous pouvez télécharger notre collection POSTMAN en [clickant ici](url).
         
    * ## 5 - OKAPI
        []()

        Ici vous pouvez retrouver la documentation de **Gat'Ape V2 OKAPI** : [okapi](https://okapi-v2.hbx.geo.francetelecom.fr/swagger-ui.html)
        Pour pouvoir effectuer des appels pour OKAPI et récupérer le jeton OKAPI, voici la marche à suivre :

        | URL  | HTTP          |
        | :---------------: |:---------------:|
        | https://okapi.rec.hbx.geo.francetelecom.fr/oauth/access_token |POST|

        **HEADERS**

        ```json
            {
                "Content-Type":"application/x-www-form-urlencoded",
                "Authorization":"Bearer VotreBearerToken"
            }
        ```

        **BODY**

        ```json
            {
                "grant_type":"client_credentials",
                "scope":"HTTPS@votreScopeDapp"
            }
        ```

        Il vous est également possible de remplir le pre-script avec POSTMAN pour automatiser votre appel OKAPI afin d'appeler une API d'un projet tiers en un seul et même appel :

        **PRE-REQUEST SCRIPT || POSTMAN**

        ```js
        const auth_request = {
            url: 'https://okapi.rec.hbx.geo.francetelecom.fr/oauth/access_token',
            method: 'POST',
                header: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer BQIAAABMAAEAB0ZUSS5ORVQAEmFtYXItdGVzdC1ob3JzcHJvZAAAAAFbyb+UAgASACCsgQ5mVYi25ziIfD4l0wDttxaBGYmbkxfIHc+BtusyAgAAADwAAQAHRlRJLk5FVAASYW1hci10ZXN0LWhvcnNwcm9kAAAAAVvJv5QCABEAELXp5Fri3OTDlyko8cefEq8AAABEAAEAB0ZUSS5ORVQAEmFtYXItdGVzdC1ob3JzcHJvZAAAAAFbyb+UAgAQABjgC2ShGr+2QDGGPuUQtkP0OHV2C+DVN2gAAAA8AAEAB0ZUSS5ORVQAEmFtYXItdGVzdC1ob3JzcHJvZAAAAAFbyb+UAgAXABCppHgF21fE8jVohV2vLBW/'
                        },
                body : {
                    mode: 'urlencoded',
                        urlencoded: [
                        {key : "grant_type", value : "client_credentials"},
                        {key :"scope", value : "HTTPS@dsp2-amar-horsprod"}]
        }}
        pm.sendRequest(auth_request, function (err, response) {
            let jsonData = response.json()
            pm.variables.set("Autorization",jsonData.access_token);
            pm.variables.set("Digest",jsonData.okapi_digest)
        });

        ```
        
    * ## 6 - Utils
        * ### A - Commandes
            []()
            **MODELS** : Commande pour la génération des Models et du DBContext depuis la base de données
            ```
            dotnet ef dbcontext scaffold "Server=WX-OR6218458\SQL2019;Database=Swit_Qualif;Trusted_Connection=True;"Microsoft.EntityFrameworkCore.SqlServer -o Entities -c SwitDBContext -p Swit.Persistence -n Swit.Domain.Entities
            ```
