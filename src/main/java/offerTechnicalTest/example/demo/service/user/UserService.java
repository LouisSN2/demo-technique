package offerTechnicalTest.example.demo.service.user;

import offerTechnicalTest.example.demo.exception.CountryNotValidException;
import offerTechnicalTest.example.demo.exception.DateNotValidException;
import offerTechnicalTest.example.demo.exception.UserAlreadyExistEception;
import offerTechnicalTest.example.demo.model.repository.UserRepository;
import offerTechnicalTest.example.demo.model.rest.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UserService {

    private static final String THECOUNTRY = "FRANCE";
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void isUserValid(User user) {
        this.isFrenchUser(user.getUserCountry());
        this.isUserBirthDateValid(user.getUserBirthDate());
        this.isUserAlreadyExist(user.getUserName());
    }

    private void isUserBirthDateValid(LocalDate date) {
        if(date.isAfter(LocalDate.now().minusYears(18)))
            throw new DateNotValidException("Sorry, you cannot create an account with us, you are not 18 year old.");
    }

    private void isFrenchUser(String country) {
        if(!country.toUpperCase().equals(THECOUNTRY)) {
            throw new CountryNotValidException("Sorry, you cannot create an account with us, you are not a French resident.");
        }
    }

    private void isUserAlreadyExist(String name) {
        if(this.userRepository.findUserByName(name) != null)
            throw new UserAlreadyExistEception("Sorry, but this user already exist, please try again or change your userName");
    }
}
