package offerTechnicalTest.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UserAlreadyExistEception extends RuntimeException{
    public UserAlreadyExistEception(String error) {
        super(error);}
}
