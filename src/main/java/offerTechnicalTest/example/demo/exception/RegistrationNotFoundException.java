package offerTechnicalTest.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RegistrationNotFoundException extends RuntimeException{
    public RegistrationNotFoundException(String error) {
        super(error);
    }
}
