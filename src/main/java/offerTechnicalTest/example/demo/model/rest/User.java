package offerTechnicalTest.example.demo.model.rest;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import javax.validation.Valid;
import java.time.LocalDate;

public class User {

    @Valid
    @NotEmpty
    @NotNull(message = "userName may not be null")
    private String userName;

    @Valid
    @NotNull(message = "userBirthdate may not be null")
    private LocalDate userBirthDate;

    @Valid
    @NotEmpty
    @NotNull(message = "userCountry may not be null")
    private String userCountry;

    private String userPhoneNumber;

    private String userGender;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public LocalDate getUserBirthDate() {
        return userBirthDate;
    }

    public void setUserBirthDate(LocalDate userBirthDate) {
        this.userBirthDate = userBirthDate;
    }
}
