package offerTechnicalTest.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CountryNotValidException extends RuntimeException{
    public CountryNotValidException(String error) {
        super(error);
    }
}
