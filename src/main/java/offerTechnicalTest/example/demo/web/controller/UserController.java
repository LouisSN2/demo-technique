package offerTechnicalTest.example.demo.web.controller;


import offerTechnicalTest.example.demo.exception.RegistrationNotFoundException;
import offerTechnicalTest.example.demo.model.entity.UserEntity;
import offerTechnicalTest.example.demo.model.repository.UserRepository;
import offerTechnicalTest.example.demo.model.rest.Registration;
import offerTechnicalTest.example.demo.model.rest.User;
import offerTechnicalTest.example.demo.service.entity.Transformer;
import offerTechnicalTest.example.demo.service.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;
    private final Transformer transformer;
    private final UserRepository userRepository;


    public UserController(UserService userService, Transformer transformer, UserRepository userRepository) {
        this.userService = userService;
        this.transformer = transformer;
        this.userRepository = userRepository;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createUser(@Valid @RequestBody User user) {
        this.userService.isUserValid(user);
        UserEntity userEntity = this.transformer.transformUserToUserEntity(user);
        this.userRepository.save(userEntity);
    }

    @GetMapping
    @ResponseStatus(code = HttpStatus.OK)
    public Registration getUserByName(@RequestParam(value = "userName", defaultValue = "null") String userName) {
        UserEntity userEntity = this.userRepository.findUserByName(userName);
        if(userEntity == null)
            throw new RegistrationNotFoundException("The user you are looking for is not present. Please try again later or change the user to be searched for.");
        return this.transformer.transformUserEntityToRegistration(userEntity);
    }


}
